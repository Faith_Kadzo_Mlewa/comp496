package com.example.kadzo_fay.four_screen_application;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void goToActivityMain2(View view){
      Intent firstActivity = new Intent(MainActivity.this,MainActivity2.class);
        startActivity(firstActivity);
    }

    public void goToActivityMain3(View view){
        Intent secondActivity = new Intent(MainActivity.this, MainActivity3.class);
        startActivity(secondActivity);
    }

    public void goToActivityMain4(View view){
        Intent thirdActivity = new Intent(MainActivity.this, MainActivity4.class);
        startActivity(thirdActivity);
    }

    public void goHome(View view){
        Intent backHomeActivity = new Intent(MainActivity.this, MainActivity.class);
        startActivity(backHomeActivity);
    }
}
