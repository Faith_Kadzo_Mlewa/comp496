package com.example.kadzo_fay.four_screen_application;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
    }
    public void goHome(View view){
        Intent firstActivity = new Intent(MainActivity2.this, MainActivity.class);
        startActivity(firstActivity);
    }
}
